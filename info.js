// let productArray = JSON.parse(localStorage.getItem("products"));

let product = JSON.parse(sessionStorage.getItem("idSelectedPr"));
let maxsulotMiqdori = document.querySelector(".maxsulotMiqdori");

const checkUserSession = sessionStorage.getItem('in');
if (!checkUserSession)
    window.location.href = 'logIn.html';

document.querySelector('#profile').innerHTML = `<p onclick='logOut()' style='cursor: pointer'>Chiqish</p>`;

function logOut() {
  sessionStorage.clear();
  window.location.href = 'logIn.html';
}

function display() {
  let img = document.querySelector(".imgCard");
  let tavsifi = document.querySelector(".tarif");
  let narxi = document.querySelector(".priceCount");
  let maxsulotTarkibi = document.querySelector(".oziqlanishQiymati");
  let maxsulotNomi = document.querySelector(".maxsulotNomi");
  let davlat = document.querySelector(".davlat");
  let hajm = document.querySelector(".hajm");
  let tarkibi = document.querySelector(".tarkib");
  let maxNomi = document.querySelector(".maxsulotNom");

    //shotini ozgartrsh kere
  let orignName = product.nom.split(' ')
  img.src = product.surat;
  tavsifi.innerHTML = product.tarif;
  narxi.innerHTML = product.narx;
  maxsulotNomi.innerHTML = product.nom;
  davlat.innerHTML = product.davlat;
  hajm.innerHTML = product.hajm;
  tarkibi.innerHTML = product.tarkib;
  //shotini ozgartrsh kere
  maxNomi.innerHTML = orignName;
  for (let i in product.oziqlanish) {
    maxsulotTarkibi.innerHTML += `<div class="left">
     <div class="b">
       <p>${i.toUpperCase()} </p>
       <p style='color: green; font-weight:bold'>${product.oziqlanish[i]} </p>
       </div>
      </div>`;
      }
}
display();