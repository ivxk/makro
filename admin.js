let productArray = localStorage.getItem("productArray")
? JSON.parse(localStorage.getItem("productArray"))
: [];


let id = 15 + productArray.length;
class Maxsulot {
  constructor(
    kategoriya,
    nom,
    narx,
    davlat,
    hajm,
    tarkib,
    uglerodParam,
    yogParam,
    belokParam,
    kaloriyaParam,
    suratPara,
    tarif
  ) {
    this.kategoriya = kategoriya;
    this.nom = nom;
    this.narx = parseInt(narx);
    this.davlat = davlat;
    this.hajm = hajm;
    this.tarkib = tarkib
    this.oziqlanish = {
      uglerod: uglerodParam,
      yog: yogParam,
      belok: belokParam,
      kaloriya: kaloriyaParam,
    };
    this.surat = suratPara ? suratPara : "https://cdn2.iconfinder.com/data/icons/e-commerce-line-4-1/1024/open_box4-512.png";
    this.tarif = tarif;
    this.id = id++;
  }
}
function maxsulotQoshish() {
  let kategoriya = document.querySelector("select[name=kategoriya]").value;
  let nom = document.getElementById("nom").value;
  let narx = document.getElementById("narx").value;
  let davlat = document.getElementById("davlat").value;
  let hajm = document.getElementById("hajm").value;
  let tarifi = document.getElementById("tarif").value;
  let uglerod = document.getElementById("uglerod").value;
  let yog = document.getElementById("yog").value;
  let belok = document.getElementById("belok").value;
  let kaloriya = document.getElementById("kaloriya").value;
  let imgUrl = document.getElementById("img").value;
  let tarkib = document.getElementById("tarkib").value;
  const product = new Maxsulot(kategoriya,nom,narx,davlat,hajm,tarkib,uglerod,yog,belok,kaloriya,imgUrl,tarkib);
  // console.log(product);
  productArray.push(product);
 
  // console.log(productArray);
  localStorage.setItem("productArray", JSON.stringify(productArray));
}
