const users = localStorage.getItem("users")
    ? JSON.parse(localStorage.getItem("users"))
    : [{username: 'admin', password: 'Admin7777'}];

function User(fsn, lsn, usn, psw){
    this.firstname = fsn;
    this.lastname = lsn;
    this.username = usn;
    this.password = psw;
}

document.querySelector('#username').addEventListener('input', ()=>{
    const p = document.getElementById('pUser');
    const usn = document.querySelector('#username').value;
    if(usn.length < 5 && usn.length > 0)
        p.innerHTML = 'Kamida 5ta belgi qatnashishi kerak';
    else p.innerHTML = '';
});

axios
  .get('https://gist.githubusercontent.com/anubhavshrimal/75f6183458db8c453306f93521e93d37/raw/f77e7598a8503f1f70528ae1cbf9f66755698a16/CountryCodes.json')
  .then(response=>{
    let codes = response.data;
    // console.log(codes);
    codes.forEach(code => {
        document.querySelector('#phoneNum').innerHTML += `
            <option value='${code.dial_code}'>${code.dial_code} ${code.name} (${code.code})</option>`
    });
  });

const checkPassword = document.getElementById('password');

checkPassword.addEventListener('input', () => {
    const val = checkPassword.value;
    const out = document.getElementById('pPassword');
    if(!val.length)
    {
        out.innerHTML = '';
    }
    else if(val.length < 8)
    {
        out.style.color = 'red';
        out.innerHTML = 'Kamida 8ta belgi qatnashishi kerak';
    }
    else if(!val.match(/[A-Z]/g))
    {
        out.style.color = 'red';
        out.innerHTML = 'Uppercase letters should be used';
    }
    else if(!val.match(/[a-z]/g))
    {
        out.style.color = 'red';
        out.innerHTML = 'Lowercase letters should be used';
    }
    else if(!val.match(/[0-9]/g))
        out.innerHTML = 'Numbers should be used';
    else
    { 
        out.innerHTML = 'Password is correct';
        out.style.color = 'green';
    }
});

document.querySelector('#reg').addEventListener('click', ()=>{
    // document.querySelector('body').style = 'background-color: #f4f4f4;'
    const fsn = document.querySelector('#firstname').value;
    const lsn = document.querySelector('#lastname').value;
    const usn = document.querySelector('#username').value;
    const psw = document.querySelector('#password').value;
    
    const check = users.find(user => {
        return user.username === usn;
    });
    if(usn.length < 5)
    {
        document.getElementById('pUser').innerHTML = 'Kamida 5ta belgi qatnashishi kerak';
    }
    else if(check){
        const p = document.getElementById('pUser');
        p.innerHTML = 'This username has already been used';
    }
    else{
        if(!(psw.length > 7 && psw.match(/[a-z]/g) && psw.match(/[A-Z]/g) && psw.match(/[0-9]/g)))
        {
            const out = document.getElementById('pPassword');
            if(psw.length < 8)
            {
                out.style.color = 'red';
                out.innerHTML = 'Kamida 8ta belgi qatnashishi kerak';
            }
            else if(!psw.match(/[A-Z]/g))
            {
                out.style.color = 'red';
                out.innerHTML = 'Uppercase letters should be used';
            }
            else if(!psw.match(/[a-z]/g))
            {
                out.style.color = 'red';
                out.innerHTML = 'Lowercase letters should be used';
            }
            else if(!psw.match(/[0-9]/g))
                out.innerHTML = 'Numbers should be used';
            else
            { 
                out.innerHTML = 'Password is correct';
                out.style.color = 'green';
            }
        }
        else
        {
            const user = new User(fsn, lsn, usn, psw);
            users.push(user);
            console.log(users);
            localStorage.setItem('users', JSON.stringify(users));
            document.querySelector('.modal').style = 'display: block';
        }
    }
});
document.querySelector('#close_btn').addEventListener('click', ()=>{
    document.querySelector('.modal').style = 'display:none';
});

const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#password');

togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
});