document.querySelector('#log-in').addEventListener('click', ()=>{
    const name = document.querySelector('#username').value;
    const password = document.querySelector('#password').value;
    const users = localStorage.getItem("users")
    ? JSON.parse(localStorage.getItem("users"))
    : [{username: 'admin', password: 'Admin7777'}];

    const bool = users.find(user => {
        return user.username === name && user.password === password;
    });
    
    if(bool)
    {
        sessionStorage.setItem('in', true), sessionStorage.setItem('user', JSON.stringify(bool));
        if(bool.username === 'admin' && bool.password === 'Admin7777')
            window.location.href = 'admin.html';
        else window.location.href = 'user.html';
    }
    else{
        document.querySelector('.modal').style = 'display: block';
    }
});

document.querySelector('.btn-close').addEventListener('click', ()=>{
    document.querySelector('.modal').style = 'display: none';
});
document.querySelector('#close_btn').addEventListener('click', ()=>{
    document.querySelector('.modal').style = 'display: none';
});

const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#password');

togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
});