// checking user profile

const checkUserSession = sessionStorage.getItem('in');
if (!checkUserSession)
    window.location.href = 'logIn.html';

const user = JSON.parse(sessionStorage.getItem('user'));

document.querySelector('#profile').innerHTML = `<p onclick='logOut()' style='cursor: pointer'>Chiqish</p>`;

function logOut() {
    sessionStorage.clear();
    window.location.href = 'logIn.html';
}

// info
const cartArray = localStorage.getItem("cartArray")
    ? JSON.parse(localStorage.getItem("cartArray"))
    : [];

let summa = 0;
cartArray.forEach((prod, id) => {
    // console.log(prod);
    summa += prod.narx;
    document.querySelector('#js_code_table').innerHTML += `
        <tr>
            <td><img src="${prod.surat}" alt="cart" class='cartProdImg'></td>
            <td>
                <p class="profile-cart-table-title">${prod.nom}</p>
                <!-- <p class="profile-cart-table-count">1 шт </p> -->
                <div class="btns">
                    <button class='prodBtn' onclick = "dif(${prod.narx}, ${id})">-</button>
                    <button class='prodBtn' id='count${id}'>1</button>
                    <button class='prodBtn' onclick = "add(${prod.narx}, ${id}, ${prod.id})">+</button>
                </div>
            </td>
            <td>
                <p>${prod.narx}  Сум/шт </p>
            </td>
            <td>
                <p>${prod.narx} <span>Сум</span></p>
            </td>
        </tr>
        `
});

function dif(price, id){
    let cnt = parseInt(document.querySelector(`#count${id}`).innerHTML);
    if(cnt > 0){    
        summa -= price;
        cnt--;
        cartArray.splice(id, 1);
        localStorage.setItem("cartArray", JSON.stringify(cartArray));
        document.querySelector(`#count${id}`).innerHTML = cnt;
        document.querySelector('#price').innerHTML = `${summa}`
        document.querySelector('.narx').innerHTML = `${summa}`
        document.querySelector('#total').innerHTML = `${summa+12000}`
    }
}

function add(price, id, id1){
    summa += price;
    cartArray.push(cartArray[id1-1]);
    localStorage.setItem("cartArray", JSON.stringify(cartArray));
    let cnt = parseInt(document.querySelector(`#count${id}`).innerHTML);
    cnt++;
    document.querySelector(`#count${id}`).innerHTML = cnt;
    document.querySelector('#price').innerHTML = `${summa}`
    document.querySelector('.narx').innerHTML = `${summa}`
    document.querySelector('#total').innerHTML = `${summa+12000}`
}

document.querySelector('.name').innerHTML = `${user.firstname} ${user.lastname}`
document.querySelector('#price').innerHTML = `${summa}`
document.querySelector('.narx').innerHTML = `${summa}`
document.querySelector('#total').innerHTML = `${summa+12000}`